package com.baomidou.mybatisplus.samples.pagination.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author miemie
 * @since 2018-09-20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParamSome {

    private Integer yihao;
    private String erhao;

    public ParamSome(Integer yihao, String erhao) {
        this.yihao = yihao;
        this.erhao = erhao;
    }

    public Integer getYihao() {
        return yihao;
    }

    public void setYihao(Integer yihao) {
        this.yihao = yihao;
    }

    public String getErhao() {
        return erhao;
    }

    public void setErhao(String erhao) {
        this.erhao = erhao;
    }
}
