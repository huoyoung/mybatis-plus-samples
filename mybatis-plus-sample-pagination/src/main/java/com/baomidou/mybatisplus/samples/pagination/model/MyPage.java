package com.baomidou.mybatisplus.samples.pagination.model;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author miemie
 * @since 2018-08-10
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class MyPage<T> extends Page<T> {
    private static final long serialVersionUID = 5194933845448697148L;

    public MyPage(long current, long size) {
        super(current, size);
    }

    private Integer selectInt;
    private String selectStr;

    public Integer getSelectInt() {
        return selectInt;
    }

    public void setSelectInt(Integer selectInt) {
        this.selectInt = selectInt;
    }

    public String getSelectStr() {
        return selectStr;
    }

    public void setSelectStr(String selectStr) {
        this.selectStr = selectStr;
    }
}
